import React, {Component} from 'react';
import {StyleSheet, Text, View, Animated, TouchableOpacity} from 'react-native';

export default class App extends Component {

  constructor(props){
    super(props)

    this.state = {
      frontTitle: 'touch for flip',
      backTitle: 'touch for flip back'
    }
  }

  componentWillMount() {
    this.animatedValue = new Animated.Value(0);
    this.value = 0;
    this.animatedValue.addListener(({ value }) => {
      this.value = value;
    })
    this.frontInterpolate = this.animatedValue.interpolate({
      inputRange: [0, 180],
      outputRange: ['0deg', '180deg']
    })
    this.backInterpolate = this.animatedValue.interpolate({
      inputRange: [0, 180],
      outputRange: ['180deg', '360deg']
    })
    this.frontOpacity = this.animatedValue.interpolate({
      inputRange: [89, 90],
      outputRange: [1, 0]
    })
    this.backOpacity = this.animatedValue.interpolate({
      inputRange: [89, 90],
      outputRange: [0, 1]
    })
  }
  flipCard() {
    if (this.value >= 90) {
      Animated.spring(this.animatedValue,{
        toValue: 0,
        friction: 8,
        tension: 10
      }).start();
    } else {
      Animated.spring(this.animatedValue,{
        toValue: 180,
        friction: 8,
        tension: 10
      }).start();
    }
  }

  render() {
    const frontAnimatedStyle = {
      transform : [
        {rotateY: this.frontInterpolate}
      ]
    }
    const backAnimatedStyle = {
      transform : [
        {rotateY: this.backInterpolate}
      ]
    }
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => this.flipCard()}>
            <Animated.View style={[styles.frontCard, frontAnimatedStyle, {opacity: this.frontOpacity}]}>
              <Text style={styles.text}>
                {this.state.frontTitle}
              </Text>
            </Animated.View>
            <Animated.View style={[styles.frontCard, styles.backCard, backAnimatedStyle, {opacity: this.backOpacity}]}>
              <Text style={styles.text}>
                {this.state.backTitle}
              </Text>
            </Animated.View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  frontCard: {
    width: 250,
    height: 125,
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backfaceVisibility: 'hidden',
  },
  backCard: {
    backgroundColor: 'blue',
    position: 'absolute',
    top: 0
  },
  text: {
    width: 90,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  }
});
